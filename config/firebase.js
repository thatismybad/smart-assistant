import admin from 'firebase-admin';
import serviceAccount from './app2doo-service_acc.json';

const config = {
    credential: admin.credential.cert(serviceAccount),
};

const init = () => admin.initializeApp(config);

export default init;
