import express from 'express';
import bodyParser from 'body-parser';
// import CryptoJS from 'crypto-js';
import init from './config/firebase';
import ThreadSetup from './helpers/threadSetup';
import verification from './controller/verification';
import message from './controller/messageWebhook';

const app = express();

init();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static('public'));

const PORT = process.env.PORT || 1337;

app.listen(PORT, () => console.log(`Webhook server is listening, port ${PORT}`));

app.get('/webhook', verification);
app.post('/webhook', message);

/*  =======================
        MESSENGER SETUP
    ======================= */
ThreadSetup.getStartedButton();
