Cílem je vytvořit aplikaci, která zastane funkci chytrého asistenta. Pomocí této aplikace uživatel bude vytvářet položky TODO/shopping listu včetně možnosti sdílet jednotlivé položky s ostatními uživateli. Správa těchto TODO položek bude implementováno za pomocí zpracování přirozeného jazyka (NLP - Natural Language Processing).
O vytváření/dokončování jednotlivých položek, které budou sdílené s nějakou skupinou budou všichni účastníci notifikováni. Aplikace bude implementována jako chatbot na platformě Facebook Messenger.
Hlavním cílem je implementovat aplikaci pro efektivní upozorňování uživatelů o jejich TODO položkách při optimalizovaném využití baterie a co nejmenším zpožděním odeslaných notifikací.

Možná budoucí rozšíření: 

- přidání příloh k jednotlivým TODO v podobě obrázků, zvuku nebo polohy; zpracování hlasu do textové podoby; 

Články na Web of Science:

- LEHVÄ, Jyri, Niko MÄKITALO a Tommi MIKKONEN. Case Study: Building a Serverless Messenger Chatbot. GARRIGÓS, Irene a Manuel WIMMER, ed. Current Trends in Web Engineering [online]. Cham: Springer International Publishing, 2018, 2018-02-22, s. 75-86 [cit. 2018-08-29]. Lecture Notes in Computer Science. DOI: 10.1007/978-3-319-74433-9_6. ISBN 978-3-319-74432-2. Dostupné z: http://link.springer.com/10.1007/978-3-319-74433-9_6

- SINNJAKROTH, Papattaranan, Varisa SARASUK, Pasachol MUSIKASINTORN, Tamonwan THUMRONGSUTTIPAN a Apirak HOONLOR. Alert Me Please: The implementation of an intelligent-time-mangement social application. In: 2014 Third ICT International Student Project Conference (ICT-ISPC) [online]. IEEE, 2014, 2014, s. 135-138 [cit. 2018-08-29]. DOI: 10.1109/ICT-ISPC.2014.6923235. ISBN 978-1-4799-5573-2. Dostupné z: http://ieeexplore.ieee.org/document/6923235/

- IKEMOTO, Yuichiro, Varit ASAWAVETVUTT, Kazuhiro KUWABARA a Hung-Hsuan HUANG. Conversation Strategy of a Chatbot for Interactive Recommendations. NGUYEN, Ngoc Thanh, Duong Hung HOANG, Tzung-Pei HONG, Hoang PHAM a Bogdan TRAWIŃSKI, ed. Intelligent Information and Database Systems [online]. Cham: Springer International Publishing, 2018, 2018-02-14, s. 117-126 [cit. 2018-08-29]. Lecture Notes in Computer Science. DOI: 10.1007/978-3-319-75417-8_11. ISBN 978-3-319-75416-1. Dostupné z: http://link.springer.com/10.1007/978-3-319-75417-8_11