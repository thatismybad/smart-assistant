import { handleMessage, handlePostback } from '../helpers/processMessage';
// import { addData } from './firebaseController';

const message = (req, res) => {
    const { body } = req;

    if (body.object === 'page') {
        body.entry.forEach((event) => {
            const messagingEvent = event.messaging[0];
            // console.log('messagingEvent', messagingEvent.sender);

            if (messagingEvent.message) {
                handleMessage(messagingEvent);
            } else if (messagingEvent.postback) {
                handlePostback(messagingEvent);
            }
        });

        res.status(200).send('EVENT_RECEIVED');
    } else {
        res.sendStatus(404);
    }
};

export default message;
