import api from './api';
import * as c from '../utils/constants';

const getStartedButton = () => {
    api.callMessengerProfileAPI(
        {
            get_started: {
                payload: c.PAYLOAD_GET_STARTED,
            },
        },
    );
};

export default {
    getStartedButton,
};
