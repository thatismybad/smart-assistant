import request from 'request';

const { PAGE_ACCESS_TOKEN } = process.env;

const callAPI = (endPoint, messageData) => {
    request({
        uri: `https://graph.facebook.com/v2.6/me/${endPoint}`,
        qs: { access_token: PAGE_ACCESS_TOKEN },
        method: 'POST',
        json: messageData,

    }, (error, response, body) => {
        if (!error && response.statusCode === 200) {
        // Message has been successfully received by Facebook
            console.log(
                `Successfully sent message to ${endPoint} endpoint: `,
                JSON.stringify(body),
            );
        } else {
            // Message has not been successfully received by Facebook
            console.error(
                `Failed calling Messenger API endpoint ${endPoint}`,
                response.statusCode,
                response.statusMessage,
                body.error,
            );
        }
    });
};

const callMessagesAPI = (messageData) => {
    return callAPI('messages', messageData);
};

const callMessengerProfileAPI = (messageData) => {
    return callAPI('messenger_profile', messageData);
};

const callThreadAPI = (messageData) => {
    return callAPI('thread_settings', messageData);
};

export default {
    callMessagesAPI,
    callMessengerProfileAPI,
    callThreadAPI,
};
