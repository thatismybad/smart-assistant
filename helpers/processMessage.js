import { Wit, log } from 'node-wit';
import api from './api';
import * as c from '../utils/constants';
import { createTodo, getTodos, completeTodo } from '../firebase/api';

const { WIT_TOKEN } = process.env;

const client = new Wit({
    accessToken: WIT_TOKEN,
    logger: new log.Logger(log.DEBUG),
});

// handles messages events
export const handleMessage = async (messagingEvent) => {
    let response;
    const { id } = messagingEvent.sender;
    const msg = messagingEvent.message;

    if (msg.text) {
        client.message(msg.text)
            .then(({ entities }) => {
                const { intent, message } = entities;
                console.log(JSON.stringify(intent));
                for (const element of intent) {
                    if (element.value === 'new_todo') {
                        const todo = {
                            title: message[0].value,
                            shared: false,
                            complete: false,
                        };
                        createTodo(id, todo);
                        response = {
                            text: `Uložil jsem tvůj úkol "${todo.title}" 🙂`,
                        };
                        api.callMessagesAPI(createResponseBody(id, response));
                    } else if (element.value === 'show_todo') {
                        let todos = '';
                        getTodos(id, 'all', (success, res) => {
                            if (success) {
                                res.map((todo, key) => {
                                    todos += `[${key + 1}] ${todo.title}\n`;
                                });
                            }
                            response = {
                                text: `Jistě, tady jsou:\n${todos}`,
                            };
                            api.callMessagesAPI(createResponseBody(id, response));
                        });
                    } else if (element.value === 'complete_todo') {
                        const taskNumber = entities.number[0].value;
                        completeTodo(id, taskNumber);
                        response = {
                            text: 'Skvělá práce!👏👏👍',
                        };
                        api.callMessagesAPI(createResponseBody(id, response));
                    }
                }
            })
            .catch(console.error);
    }
};

const createResponseBody = (id, message) => {
    return {
        recipient: {
            id,
        },
        message,
    };
};

// sends response from tapped postback
const sendPostbackResponse = (recipient, response) => {
    const responseBody = {
        recipient: {
            id: recipient,
        },
        message: response,
    };
    api.callMessagesAPI(responseBody);
};

// handles messaging_postbacks events
export const handlePostback = (messagingEvent) => {
    let response;

    const { payload } = messagingEvent.postback;
    console.log('handlePostback', payload);

    switch (payload) {
    case c.PAYLOAD_GET_STARTED:
        response = { text: 'Ahoj! Vrhněme se na velká 2doočka 😊' };
        break;
    default:
        response = {
            text: 'I really don\'t know what should I say..',
        };
        break;
    }

    sendPostbackResponse(messagingEvent.sender.id, response);
};
