export const PAYLOAD_CREATE_TODO = 'create_todo';
export const PAYLOAD_OWNED_TODOS = 'owned_todos';
export const PAYLOAD_SHARED_TODOS = 'shared_todos';
export const PAYLOAD_GET_STARTED = 'get_started';
export const PAYLOAD_REGISTER = 'register';
