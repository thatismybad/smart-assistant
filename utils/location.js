const deg2rad = deg => deg * (Math.PI / 180);

const getDistanceInMetres = (l1, l2) => {
    const decimals = 3;
    const R = 6371;
    const dLat = deg2rad(l2.lat - l1.lat);
    const dLon = deg2rad(l2.lon - l1.lon);
    const a = (
        Math.sin(dLat / 2) * Math.sin(dLat / 2)
        + Math.cos(deg2rad(l1.lat)) * Math.cos(deg2rad(l2.lat))
        * Math.sin(dLon / 2) * Math.sin(dLon / 2)
    );
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    const d = (R * c) * 1000;
    return Math.round(d * (10 ** decimals)) / (10 ** decimals);
};

export default getDistanceInMetres;
