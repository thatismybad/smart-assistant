export const TITLE_CREATE_TODO = 'Create New ToDo';
export const TITLE_MY_TODOS = 'My ToDos';
export const TITLE_SHARED_WITH_ME = 'Shared With Me';
export const TITLE_GET_STARTED = 'Get Started';
