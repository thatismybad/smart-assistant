import admin from 'firebase-admin';
import * as c from './constants';

// create todo function
export const createTodo = (psid, todo) => {
    const db = admin.firestore();
    const { title, shared, complete } = todo;

    const todoRef = db.collection(c.COLLECTION_USERS)
        .doc(psid)
        .collection(c.COLLECTION_TODOS)
        .doc();
    todoRef.set({
        id: todoRef.id,
        title,
        shared,
        complete,
    });
};

export const getTodos = (psid, todoType = 'all', callback) => {
    const db = admin.firestore();
    const todos = [];

    const todosRef = db.collection(c.COLLECTION_USERS)
        .doc(psid)
        .collection(c.COLLECTION_TODOS)
        .get();

    todosRef.then((snapshot) => {
        snapshot.forEach((doc) => {
            const todo = doc.data();
            if (!todo.complete) {
                switch (todoType) {
                case 'my':
                    if (!todo.shared) {
                        todos.push(todo);
                    }
                    break;
                case 'shared':
                    if (todo.shared) {
                        todos.push(todo);
                    }
                    break;
                default:
                    todos.push(todo);
                    break;
                }
            }
        });
        callback(true, todos);
    }).catch(error => callback(false, error));
};

export const completeTodo = (psid, todoNumber) => {
    const db = admin.firestore();

    getTodos(psid, 'all', (success, res) => {
        if (success) {
            if (todoNumber <= res.length || todoNumber > 1) {
                const todoId = res[todoNumber - 1].id;
                db.collection(c.COLLECTION_USERS)
                    .doc(psid)
                    .collection(c.COLLECTION_TODOS)
                    .doc(todoId)
                    .update({
                        complete: true,
                    });
            }
        }
    });
};
